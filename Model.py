import sys
import subprocess
import numpy as np
import copy
import json

# search path for online model
sys.path.append('/sf/bd/applications/OnlineModel/current')



#import OMFacility
import OMWakes
import OMDWakes

#from OMMadxLatticeLight import *

class Model:
    def __init__(self):
        
        self.wakes=OMWakes.Wakes()
        self.dechirp=OMDWakes.Dechirper()
        # common line
        self.line=[]
        self.line.append(self.addChicane('Laser Heater','SINLH01','SINLH03',2.22e-3))        
        self.line.append(self.addRF('SINSB'   ,'SINSB03','SINSB04',16.32*4*4.06,70.55,'S',4*4.083))
        self.line.append(self.addRF('SINXB'   ,'SINXB01','SINXB01',15.96*2*0.75,270.,'X',2*0.75))
        self.line.append(self.addChicane('BC1','SINBC01','SINDI02',63.42e-3))
        self.line.append(self.addRF('Linac1-1','S10CB01','S10DI01',450,69,'C',2*4*1.978))
        self.line.append(self.addRF('Linac1-2','S10CB03','S10CB09',1400.,69,'C',7*4*1.978))
        self.line.append(self.addChicane('BC2','S10BC01','S10MA01',20.80e-3))
        self.line.append(self.addRF('Linac2'  ,'S20CB01','S20CB04',1100.,90,'C',4*4*1.978))
        self.lineAR=[]
        self.lineAR.append(self.addRF('Linac3'  ,'S30CB01','S30CB14',2800.,90,'C',13*4*1.978))
        self.lineAR.append(self.addChicane('AR-ECOL','SARCL01','SARMA01',0))
        self.lineAT=[]
        self.lineAT.append(self.addChicane('Switch Yard','S20SY02','SATDI01',-3.5e-3))            
        self.lineAT.append(self.addWake('AT-Dechirper','S20CL02','S20MA01',1.5e-3,8))
        self.oldDestination=-1
        self.ncommon=len(self.line)
        self.buildModel(0)  # build  branch

        self.s=None
        self.cur=None
        self.wpot=None
        self.Q=0
        self.N=100



    def save(self,filename):
        data={}
        for ele in self.line:
            data[ele['Section']]=ele
        data['Destination']=self.oldDestination
        with open(filename,'w') as outfile:
                     json.dump(data,outfile,indent=4, sort_keys=True)
         
    def load(self,filename):
        with open(filename,'r') as infile:
            data=json.load(infile)
            self.buildModel(data['Destination'])
            for key in data.keys():
                if 'Destination' in key:
                    continue
                else:
                    for i,ele in enumerate(self.line):
                        if key in ele['Section']:
                            self.line[i]=data[key]
                
            
        


    def track(self,dist,destination,Q,N):

        if dist.hasDistribution is False:
            return

        dist.getCurrent(Q,N)
        self.s  =dist.xslice
        self.cur=dist.yslice
        self.Q=Q
        self.N=N

        iloc=-1
        for i in range(len(self.line)):
            if dist.location in self.line[i]['Section']:
                iloc=i
                break

        itar=-1
        if isinstance(destination, int):
            itar=iloc+destination
        else:
            for i in range(len(self.line)):
                if destination in self.line[i]['Section']:
                    itar=i
                    break

        if itar >= len(self.line):
            itar=len(self.line)-1

        if itar < -1:
            itar=-1

        delta=itar-iloc

        if delta is 0:
            return

        if delta > 0:
            for i in range(delta):
                self.trackElement(dist,self.line[iloc+1+i],False)
                dist.location= self.line[iloc+1+i]['Section']
                self.line[iloc+1+i]['Energy']=np.mean(dist.y)
        else:
            for i in range(-delta):
                self.trackElement(dist,self.line[iloc-i],True)
                if (iloc-i-1) < 0:
                    dist.location= 'Start'
                else:
                    dist.location= self.line[iloc-i-1]['Section']
        
        return


    def trackElement(self,dist,ele,Backward=False):
        if 'Chicane' in ele['Type']:
            self.applyR56(ele,dist,Backward)
            dist.getCurrent(self.Q,self.N)
            self.s  =dist.xslice
            self.cur=dist.yslice
            self.wpot=self.s*0
        elif 'RF' in ele['Type']:
            self.applyRF(ele,dist,Backward)
            self.applyRFWake(dist,ele['Band'],ele['Length'],Backward)
        elif 'Dechirper' in ele['Type']:
            self.applyDechirperWake(dist,ele['Gap'],ele['Length'],Backward)
        else:
            print('Unknown element', ele)                    


     
    def applyR56(self,ele,dist, Backward=False):
        Eref=ele['Ref']
        E0=np.mean(dist.y)
        if Eref >= 0 :
            E0=Eref
        dE=dist.y/E0-1
        R=-ele['R56']
        T=-ele['T566']
        U=-ele['U5666']
        scl=1
        if Backward:
            scl=-1
        dist.x+=(R+(T+U*dE)*dE)*dE*scl

        
    def applyRF(self,ele,dist,Backward=False):
        krf=2*np.pi*ele['Freq']/3e8
        phi=np.pi*ele['Phase']/180+krf*dist.x
        scl=1
        if Backward:
            scl=-1
        dist.y+=ele['Voltage']*scl/0.511*np.sin(phi)

    def applyRFWake(self,dist,band,L,Backward=False):
        s=self.s-np.min(self.s)
        dt=(s[1]-s[0])/3e8
        self.wakes.getWake(band)
        wpot=-np.interp(s,self.wakes.s,self.wakes.lwake)
        wpot[0]*=0.5
        wfull=np.convolve(self.cur,wpot)*dt
        self.wpot=wfull[0:len(s)]
        scl=1
        if Backward:
            scl=-1
        dist.y+=np.interp(dist.x,self.s,self.wpot)*L/511000*scl

    def applyDechirperWake(self,dist,gap,L,Backward=False):
        s=self.s-np.min(self.s)
        dt=(s[1]-s[0])/3e8
        self.dechirp.getWake(gap)
        wpot=-np.interp(s,self.dechirp.s,self.dechirp.lwake)
        wpot[0]*=0.5
        wfull=np.convolve(self.cur,wpot)*dt
        self.wpot=wfull[0:len(s)]
        scl=1
        if Backward:
            scl=-1
        dist.y+=np.interp(dist.x,self.s,self.wpot)*L/511000*scl


        


    def buildModel(self,destination):   
        if destination is self.oldDestination:
            return

        while len(self.line) > self.ncommon:
            self.line.pop()
        
        if destination is 0:
            for ele in self.lineAR:
                self.line.append(ele)
        else:
            for ele in self.lineAT:
                self.line.append(ele)

        self.oldDestination=destination


    def addChicane(self,section,start,end,R56):
        ele={'Type':'Chicane'}
        ele['Section']=section
        ele['Start']  =start
        ele['End']    =end
        ele['Energy'] = -1
        ele['R56']=R56
        ele['T566']=-3*R56/2
        ele['U5666']=2*R56
        ele['Ref']=-1
        return ele

    def addRF(self,section,start,end,V,phi,band,L):
        f0=5.712e9
        if 'S' in band:
            f0=2.9988e9
        if 'X' in band:
            f0=11.9952e9   
        ele={'Type':'RF'}
        ele['Section']=section
        ele['Start']  =start
        ele['End']    =end
        ele['Energy'] = -1
        ele['Voltage']=V
        ele['Phase']  =phi
        ele['Freq']   =f0        
        ele['Band']   =band
        ele['Length'] =L

        return ele
        
    def addWake(self,section,start,end,gap,L):
        ele={'Type':'Dechirper'}
        ele['Section']=section
        ele['Start']  =start
        ele['End']    =end
        ele['Energy'] = -1
        ele['Gap'] = gap
        ele['corrugation']=1
        ele['Length'] =L
        ele['Ref']=-1
        return ele
