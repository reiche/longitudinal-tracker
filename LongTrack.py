import sys
import copy
#import datetime
#import time

import numpy as np
from scipy import stats
import h5py

#import math
#import webbrowser

from PyQt5 import QtGui,QtCore,QtWidgets
from PyQt5.uic import loadUiType


from matplotlib.figure import Figure
from matplotlib import gridspec
import matplotlib.patches as patches
import matplotlib.pyplot as plt

from matplotlib.backends.backend_qt4agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)


import Distribution
import Model
import EPICSInterface
import Compression

Ui_LongTrackGUI, QMainWindow = loadUiType('LongTrack.ui')



class LongTrack(QMainWindow,Ui_LongTrackGUI):
    def __init__(self,):
        super(LongTrack,self).__init__()

        self.version='1.0.0'
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon("rsc/iconcompression.png"))

        self.dist=Distribution.Distribution()
        self.data=None

        self.model=Model.Model()
        self.EPICS=EPICSInterface.EPICSInterface()
        self.comp=Compression.Compression()

        self.initmpl()

        self.ImportList.addItem('Start')
        self.TrackList.addItem('Start')
        for ele in self.model.line:
            self.TrackList.addItem(ele['Section'])
            self.ImportList.addItem(ele['Section'])
        

        self.ImportElegant.clicked.connect(self.importdist)
        self.ImportMeasurement.clicked.connect(self.importdist)
        self.ImportGeneric.clicked.connect(self.generatedist)
        self.NextImage.clicked.connect(self.incPlotMeasurement)
        self.PrevImage.clicked.connect(self.decPlotMeasurement)
        self.ConvertMeasurement.clicked.connect(self.convertDist)
        self.Flipx.toggled.connect(self.plotMeasurement)
        self.Flipy.toggled.connect(self.plotMeasurement)

        self.Track.clicked.connect(self.doTrack)
        self.TrackNext.clicked.connect(self.doTrack)
        self.TrackPrev.clicked.connect(self.doTrack)
        self.Reset.clicked.connect(self.resetDist)
        self.Save.clicked.connect(self.saveDist)

        self.PlotDist.clicked.connect(self.plot)
        self.PlotDist2D.clicked.connect(self.plot2D)
        self.PlotCurrent.clicked.connect(self.plotCurrent)
        self.PlotWakes.clicked.connect(self.plotWakes)
        self.PlotEnergy.clicked.connect(self.plotEnergyDistribution)
        self.AddRefPlot.clicked.connect(self.plotAddRef)
        self.DeleteRefPlot.clicked.connect(self.plotDelRef)
        
        self.UpdateModel.clicked.connect(self.updateLattice)
        self.ReadMachine.clicked.connect(self.machine2Lattice)
        self.Destination.currentIndexChanged.connect(self.changeBeamline)
        self.Collapse.toggled.connect(self.updateLatticeTable)
        self.updateLatticeTable()

        self.CompCalc.clicked.connect(self.calcCompression)
        self.UILoadSettings.clicked.connect(self.load)
        self.UISaveSettings.clicked.connect(self.save)

        self.refPlots=[]


#-------------
# load and save settings

    def load(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Settings",
                                                            "/sf/data/applications/BD-LongTrack/Settings",
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return
        self.model.load(fileName)
        self.updateLatticeTable()    
        self.Destination.setCurrentIndex(self.model.oldDestination)
 

    def save(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save Settings",
                                                            "/sf/data/applications/BD-LongTrack/Settings",
                                                            "Json Files (*.json)", options=options)
        if not fileName:
            return

        self.model.save(fileName)


#---------------------------
# optimize compresison
    def calcCompression(self):

        E1=float(str(self.CompE1.text()))
        E2=float(str(self.CompE2.text()))
        E3=float(str(self.CompE3.text()))
        Cur  =float(str(self.CompCurrent.text()))
        Chirp=float(str(self.CompChirp.text()))*1e-2
        Quad =float(str(self.CompQuad.text()))*1e-2
        Q=float(str(self.Charge.text()))*1e-12
        R561=float(str(self.CompBC1R56.text()))*1e-3
        R562=float(str(self.CompBC2R56.text()))*1e-3
        doR56=self.CompSetR56.isChecked()
        self.comp.calc(self.dist,self.model,[E1,E2,E3],[Q,Cur,Chirp,Quad],[R561,R562],doR56)
        self.updateLatticeTable()

#-------------------
# distribution event handler
    def doTrack(self):
        name=self.sender().objectName()
        loc=str(self.TrackList.currentText())   
        Q=float(str(self.Charge.text()))*1e-12
        N=int(str(self.NSlices.text()))

        if 'Next' in name:
            self.model.track(self.dist,1,Q,N)
        elif 'Prev' in name:
            self.model.track(self.dist,-1,Q,N)
        else:
            self.model.track(self.dist,loc,Q,N)
        self.updateLatticeTable()
        self.plot()


    def resetDist(self):
        self.dist.resetDistribution()
        self.plot()

    def saveDist(self):
        self.dist.setReference()


    def changeBeamline(self):
        self.model.buildModel(self.Destination.currentIndex())
        self.dist.resetDistribution()
        self.updateLatticeTable()

    def machine2Lattice(self):

        root = self.Lattice.invisibleRootItem()
        domain_count = root.childCount()
        for idx in range(domain_count):
            domain = root.child(idx)
            domaintxt = str(domain.text(0))
            chk=domain.checkState(0)
            if chk>0:                
                res=self.EPICS.readMachine(domaintxt)
                if res is None:
                    continue
                else:
                    if 'RF' in self.model.line[idx]['Type']:
                        self.model.line[idx]['Voltage']=res[0]
                        self.model.line[idx]['Phase']  =res[1]
                    elif 'Chicane' in self.model.line[idx]['Type']:
                        self.model.line[idx]['R56']  =-float(res[0])
                        self.model.line[idx]['T566'] =-float(res[1])
                        self.model.line[idx]['U5666']=-float(res[2])
        self.updateLatticeTable()
        return 

    def updateLattice(self):


        root = self.Lattice.invisibleRootItem()
        domain_count = root.childCount()
        for idx in range(domain_count):
            domain = root.child(idx)
            domaintxt = str(domain.text(0))
            chk=domain.checkState(0)
            if chk>0:                
                child_count=domain.childCount()
                for j in range(child_count):
                    child=domain.child(j)
                    chk2=child.checkState(0)
                    if chk2 > 0 :
                        tag = str(child.text(0))
                        val = str(child.text(1))
                        if 'Ref' in tag:
                            tag='Ref'
                            if 'Auto' in val:
                                val = '-1'
                            else:
                                val=float(val)/0.511
                        if not 'Structure' in tag:
                            val=float(val)
                        self.model.line[idx][tag]=val    
        self.updateLatticeTable()
        self.resetDist()
        return 



    def updateLatticeTable(self):

        self.Lattice.clear()
        header = QtWidgets.QTreeWidgetItem(["Sensors","Value","Energy"])
        self.Lattice.setHeaderItem(header)   #Another alternative is setHeaderLabels(["Tree","First",...])
        nrow=len(self.model.line)      
        for idx in range(nrow):
            erg=self.model.line[idx]['Energy']
            if erg < 0:
                estr='---'
            else:
                estr='%6.2f' % (erg*0.511)
            root = QtWidgets.QTreeWidgetItem(self.Lattice, [self.model.line[idx]['Section'],'',estr])
            root.setFlags(root.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable )
            root.setCheckState(0,QtCore.Qt.Checked)

            if 'RF' in self.model.line[idx]['Type']:
                child = QtWidgets.QTreeWidgetItem(root, ['Voltage',str('%6.2f' % self.model.line[idx]['Voltage'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
                child = QtWidgets.QTreeWidgetItem(root, ['Phase',str('%6.2f' % self.model.line[idx]['Phase'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)

            if 'Chicane' in self.model.line[idx]['Type']:
                child = QtWidgets.QTreeWidgetItem(root, ['R56',str('%6.2e' % self.model.line[idx]['R56'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
                child = QtWidgets.QTreeWidgetItem(root, ['T566',str('%6.2e' % self.model.line[idx]['T566'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
                child = QtWidgets.QTreeWidgetItem(root, ['U5666',str('%6.2e' % self.model.line[idx]['U5666'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
                if self.model.line[idx]['Ref'] < 0:
                    child = QtWidgets.QTreeWidgetItem(root, ['Ref. Energy',str('Auto')])
                    child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                    child.setCheckState(0,QtCore.Qt.Checked)
                else:
                    child = QtWidgetsy.QTreeWidgetItem(root, ['Ref. Energy',str('%6.2f' % (self.model.line[idx]['Ref']*0.511))])
                    child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                    child.setCheckState(0,QtCore.Qt.Checked)


            if 'Dechirper' in self.model.line[idx]['Type']:
                child = QtWidgets.QTreeWidgetItem(root, ['Gap',str('%6.2e' % self.model.line[idx]['Gap'])])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
                child = QtWidgets.QTreeWidgetItem(root, ['Structure','Athos'])
                child.setFlags(child.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEditable)
                child.setCheckState(0,QtCore.Qt.Checked)
            
    
        self.Lattice.header().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.Lattice.header().setStretchLastSection(False)  
        if self.Collapse.isChecked():
            self.Lattice.collapseAll()
        else:
            self.Lattice.expandToDepth(2)

        return

#-------------------
# distribution event handler

    def generatedist(self):
        dist=str(self.GenericDist.currentText())
        rfcurve=self.GenericRFCurvature.isChecked()
        duration=float(str(self.GenericRMSLength.text()))
        loc=str(self.ImportList.currentText())   
        stat=self.dist.generateDist(duration,rfcurve,dist,loc,200000)
        if stat:
            self.NTeilchen.setText("%d" % self.dist.x.shape[0])
            self.plot()
    

    def importdist(self):
        name=self.sender().objectName()



        if 'Elegant' in name:
            dirpath= "/sf/bd/simulations/Reference/Input-Distribution"
            format = "SDDS Files (*.sdds)"
        if 'Measure' in name:
            dirpath= "/sf/data/measurements/2019/06/10"
            format = "HDF5 Files (*.h5)"


        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open Distribution", dirpath, format, options=options)
        if not fileName:
            return
        stat=False
        if 'Elegant' in name:
            loc=str(self.ImportList.currentText())   
            heating=-1
            if self.DoHeating.isChecked():
                heating=float(str(self.Heating.text()))
            stat=self.dist.importElegant(fileName,loc,heating)
        if 'Measurement' in name:
           self.prepareMeasurement(fileName)
           

        if stat:
            self.NTeilchen.setText("%d" % self.dist.x.shape[0])
            self.plot()
    
        




#    def openWiki(self):
#        webbrowser.open('https://acceleratorwiki.psi.ch/wiki/Undulator_Pointing_Anpassung')


#------------------------
# initializing matplotlib
    def initmpl(self):
        self.fig=Figure()
        self.axes=self.fig.add_subplot(111)
        self.canvas = FigureCanvas(self.fig)
        self.mplvl.addWidget(self.canvas)
        self.canvas.draw()
        self.toolbar=NavigationToolbar(self.canvas,self.mplwindow, coordinates=True)
        self.mplvl.addWidget(self.toolbar)

#--------
# plotting


    def prepareMeasurement(self,filename):
        self.data=None
        with h5py.File(filename,"r") as f:
            self.data=np.array(f['Raw_data']['Beam images'])
            self.disp=f['Input']['Dispersion'][()]
            self.menergy=f['Input']['Energy'][()]
            self.xdata=np.array(f['Raw_data']['xAxis'])/self.disp*1e-6*1e2
            self.ydata=np.array(f['Meta_data']['Time axes'])
        if self.data is None:
            return

        self.measidx=0
        self.MeasPar.setEnabled(True)
        self.plotMeasurement()
        self.xbbsave=self.axes.get_xlim()
        self.ybbsave=self.axes.get_ylim()
        
    def incPlotMeasurement(self):
        self.measidx+=1
        self.plotMeasurement()

    def decPlotMeasurement(self):
        self.measidx-=1
        self.plotMeasurement()

    def convertDist(self):
        if self.data is None:
            return

        i2= self.measidx % self.data.shape[1]
        i1= int((self.measidx-i2)/self.data.shape[1]) % self.data.shape[0]
        image=self.data[i1,i2,:,:]

        flpx=self.Flipx.isChecked()
        flpy=self.Flipy.isChecked()
        if flpy:
            image=np.fliplr(image)
        if flpx:
            image=np.flipud(image)


        ybb=self.axes.get_xlim()  # since the plot is transposed !!!!!
        xbb=self.axes.get_ylim()
        
#        print('Image subsection')
#        print('Raw Image size:', image.shape)
#        print('X-data Size:',self.xdata.shape)
#        print('Y-data Size:',self.ydata.shape)
#        print('X-limits (plot):',xbb)
#        print('X-limits (data): (',np.min(self.xdata),',',np.max(self.xdata),')')
#        print('Y-limits (plot):',ybb)
#        print('X-limits (data): (',np.min(self.ydata),',',np.max(self.ydata),')')


        dx=np.max(self.xdata)-np.min(self.xdata)
        ix1=int(np.round((np.min(xbb)-np.min(self.xdata))/dx*(self.xdata.shape[0])))
        ix2=int(np.round((np.max(xbb)-np.min(self.xdata))/dx*(self.xdata.shape[0])))
#        print('X-slicing:',ix1,ix2)

        dy=np.max(self.ydata)-np.min(self.ydata)
        iy1=int(np.round((np.min(ybb)-np.min(self.ydata))/dy*(self.ydata.shape[0])))
        iy2=int(np.round((np.max(ybb)-np.min(self.ydata))/dy*(self.ydata.shape[0])))
#        print('Y-slicing:',iy1,iy2)
        image=np.fliplr(image)
        image=image[iy1:iy2,ix1:ix2]

        Nsam=int(str(self.NPart.text()))
        Thold=float(str(self.Thold.text()))
        loc=str(self.ImportList.currentText())   
        self.dist.importMeasurement(image,ybb,xbb,Nsam,Thold,self.menergy,loc)
        self.plot()


    def plotMeasurement(self):
        if self.data is None:
            return
        i2= self.measidx % self.data.shape[1]
        i1= int((self.measidx-i2)/self.data.shape[1]) % self.data.shape[0]
        flpx=self.Flipx.isChecked()
        flpy=self.Flipy.isChecked()

        image=self.data[i1,i2,:,:]
        if flpy:
            image=np.fliplr(image)
        if flpx:
            image=np.flipud(image)
        self.axes.clear()
        bbox=[np.min(self.ydata), np.max(self.ydata), np.min(self.xdata), np.max(self.xdata)]
        im=self.axes.imshow(np.transpose(image),aspect='auto',interpolation='none',cmap='viridis',extent=bbox)
        self.axes.set_xlabel('t (fs)')
        self.axes.set_ylabel(r'$\Delta$E/E (%)')
        self.axes.set_title('Measurement of Longitudinal Phase Space')
        self.canvas.draw()

    def plotCurrent(self):
        if self.dist.hasDistribution is False:
            return
        Q=float(str(self.Charge.text()))*1e-12
        N=int(str(self.NSlices.text()))
        self.dist.getCurrent(Q,N)
        self.axes.clear()
        x=self.dist.xslice*1e15/3e8
        y=self.dist.yslice

        x0=np.sum(y)
        x1=np.sum(x*y)/x0
        x2=np.sum(x*x*y)/x0
        sigt=r'$\sigma_{t} = %7.2f$ fs' % (np.sqrt(np.abs(x2-x1*x1)))
        self.axes.plot(x,y)
        self.axes.text(0.02,0.95,sigt,transform=self.axes.transAxes,fontsize=12)
        self.axes.set_xlabel('t (fs)')
        self.axes.set_ylabel('I (A)')
        self.axes.set_title('Current Profile at %s' % self.dist.location)
        self.canvas.draw()

    def plotEnergyDistribution(self):
        if self.dist.hasDistribution is False:
            return
        N=int(str(self.NSlices.text()))
        self.dist.getEnergyDistribution(N)
        self.axes.clear()
        x=self.dist.xslice*0.511
        y=self.dist.yslice
        x0=np.sum(y)
        x1=np.sum(x*y)/x0
        x2=np.sum(x*x*y)/x0
        sigE= np.sqrt(np.abs(x2-x1*x1))/x1*100
        sigt=r'$\sigma_{E} = %7.2f$ (%%)' % sigE
        self.axes.plot(x,y)
        self.axes.text(0.02,0.95,sigt,transform=self.axes.transAxes,fontsize=12)
        self.axes.set_xlabel('E (MeV)')
        self.axes.set_ylabel('p(E)')
        self.axes.set_title('Energy Distribution at %s' % self.dist.location)
        self.canvas.draw()


 
    def plotWakes(self):
#        if self.dist.hasDistribution is False:
#            return
        self.axes.clear()
        if self.model.wpot is None:
            return
        x=self.model.s*1e6
        y=self.model.wpot
        self.axes.plot(x,y*1e-3)
        self.axes.set_xlabel('s (micron)')
        self.axes.set_ylabel('Wake (kV/m)')
        self.axes.set_title('Wake Potential for %s' % self.dist.location)
        self.canvas.draw()
 

    def plot2D(self):
        if self.dist.hasDistribution is False:
            return
        self.axes.clear()
        x=self.dist.x*1e6
        y=self.dist.y*0.511


        xmin = x.min()
        xmax = x.max()
        ymin = y.min()
        ymax = y.max()

        N=1000
        
        xn=np.floor((x-xmin)/(xmax-xmin)*(N-1))
        yn=np.floor((y-ymin)/(ymax-ymin)*(N-1))
        img=np.zeros((N,N))

        for i in range(len(xn)):
            img[int(xn[i]), int(yn[i])]+=1

        im=self.axes.imshow(np.transpose(np.fliplr(img)),aspect='auto',interpolation='none',cmap='viridis',extent=[xmin, xmax, ymin, ymax])
        self.axes.set_xlabel('s (micron)')
        self.axes.set_ylabel('E (MeV)')
        self.axes.set_title(self.dist.location)
        self.canvas.draw()
        
 

    def plot(self):
        if self.dist.hasDistribution is False:
            return
        self.axes.clear()
        x=self.dist.x*1e6
        y=self.dist.y*0.511
        per=self.Epercent.isChecked()
        if per:
            ymean=np.mean(y)
            y=(y-ymean)/ymean*100
        self.axes.scatter(x,y,s=0.1)

        for ele in self.refPlots:
            x=ele[0]*1e6
            y=ele[1]*0.511
            if per:
                ymean=np.mean(y)
                y=(y-ymean)/ymean*100
            self.axes.scatter(x,y,s=0.1)

        self.axes.set_xlabel('s (micron)')
        if per:
            self.axes.set_ylabel('E (%)')
        else:
            self.axes.set_ylabel('E (MeV)')

        self.axes.set_title(self.dist.location)
        self.canvas.draw()
        
   
    def plotAddRef(self):
        if self.dist.hasDistribution is False:
            return
        self.refPlots.append([copy.deepcopy(self.dist.x),copy.deepcopy(self.dist.y)])
        self.RefPlot.addItem(str('%s' % self.dist.location))

    def plotDelRef(self):
        print('Hello World')
        idx=self.RefPlot.currentIndex()
        self.RefPlot.removeItem(idx)
        self.refPlots.pop(idx)
        


if __name__ == '__main__':
    QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("plastique"))
    app=QtWidgets.QApplication(sys.argv)
    main=LongTrack()
    main.show()
    sys.exit(app.exec_())
