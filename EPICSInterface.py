from epics import PV
import numpy as np
import sys
import os

sys.path.append('/sf/bd/applications/OnlineModel/current')

#from OMAppTemplate import ApplicationTemplate
#from OMMadxLatticeLight import *
import OMFacility
import OMElegant as Elegant
#import OMDistribution as Distribution


class EPICSInterface:
    def __init__(self):

        self.elegant = Elegant.Elegant()
        self.Facility = OMFacility.Facility(1, 0)  # initialize the lattice
        sec = self.Facility.getSection('SARBD01')
        path = sec.mapping
        self.line = self.Facility.BeamPath(path)

        # RF
        rf=[]
        for i in range(3,5):
            rf.append('SINSB%2.2d' % i)
        rf.append('SINXB01')
        for i in range(1,10):
            rf.append('S10CB%2.2d' % i)
        for i in range(1,5):
            rf.append('S20CB%2.2d' % i)
        for i in range(1,14):
            rf.append('S30CB%2.2d' % i)




    def readMachine(self,section):
        if 'SINSB' in section:
            return self.readRF(['SINSB%2.2d' % i for i in range(3,5)])
        if 'SINXB' in section:
            return self.readRF(['SINXB%2.2d' % i for i in range(1,2)])
        if 'Linac1-1' in section:
            return self.readRF(['S10CB%2.2d' % i for i in range(1,3)])
        if 'Linac1-2' in section:
            return self.readRF(['S10CB%2.2d' % i for i in range(3,10)])
        if 'Linac2' in section:
            return self.readRF(['S20CB%2.2d' % i for i in range(1,5)])
        if 'Linac3' in section:
            return self.readRF(['S30CB%2.2d' % i for i in range(1,14)])
        if 'Laser' in section:
            return self.readChicane('SINLH02','SINLH02')
        if 'BC1' in section:
            return self.readChicane('SINBC02','SINBC02')
        if 'BC2' in section:
            return self.readChicane('S10BC02','S10BC02')
        if 'AR-ECOL' in section:
            return self.readChicane('SARCL02','SARCL02')
        if 'Switch' in section:
            return self.readChicane('S20SY02','SATDI01')
        


    def readMagnet(self,start):

        #find magnets
        bendlist=self.Facility.listElement(start+'*MBND')
        for ele in bendlist:
            if 'MBND100' in ele or 'SATSY01' in ele:
                if 'SINLH' in start or 'SINBC' in start or 'S10BC' in start:
                    pv=PV('%s:ANGLE-CALC' % ele.replace('.','-'))
                else:
                    pv=PV('%s:BEND-ANGLE' % ele.replace('.','-'))
                angle=pv.get()
                if 'SATSY01' in start:
                    self.Facility.setRegExpElement(ele.split('.')[0],ele.split('.')[1],'angle',angle)
                else:
                    self.Facility.setRegExpElement(ele.split('.')[0],'MBND','angle',angle)                    
                print(ele,':',angle)
        quadlist=self.Facility.listElement(start+'*MQUA')
        for ele in quadlist:
            if 'MQUA' in ele:
                pv=PV('%s:K1L-SET' % ele.replace('.','-'))
                k1l=pv.get()
                ep=self.Facility.getElement(ele)
#                print(ele,':',k1l,ep.k1*ep.Length)
                ep.k1=k1l/ep.Length


        sextlist=self.Facility.listElement(start+'*MSEX')
        for ele in sextlist:
            if 'MSEX' in ele:
                pv=PV('%s:K2L-SET' % ele.replace('.','-'))
                k2l=pv.get()
                ep=self.Facility.getElement(ele)
#                print(ele,':',k2l,ep.k2*ep.Length)
                ep.k2=k2l/ep.Length


    def readChicane(self,start,end):
        if 'SY' in start:
            self.readMagnet('S20SY02')
            self.readMagnet('SATSY01')
            self.readMagnet('SATSY02')
            self.readMagnet('SATSY03')
            self.readMagnet('SATCL01')
        else:
            self.readMagnet(start)
        
        res=self.runElegant(start,end)
        return res

    
    def readRF(self,section):
        group = []
        for ele in section:
            group.append(PV('%s-RSYS:REQUIRED-OP'% ele))
            group.append(PV('%s-RSYS:GET-BEAM-PHASE' % ele))
            group.append(PV('%s-RSYS:GET-ACC-VOLT'% ele))
        

        res =[p.get() for p in group]        
        if None in res:
            self.completeRead=False
            l=[i for i,v in enumerate(res) if v == None]
            for idx in l:
                print('Cannot read from PV: %s' % group[idx].pvname)
            return None   
        else:
            csum=0
            ssum=0
            for i,ele in enumerate(section):
                if res[3*i] is 1:
                    amp=res[3*i+2]
                    pha=res[3*i+1]
                else:
                    amp=0
                    pha=0
                csum+=amp*np.cos(pha/180*np.pi)
                ssum+=amp*np.sin(pha/180*np.pi)
        phase=np.arctan2(ssum,csum)*180/np.pi
        amp=np.sqrt(csum*csum+ssum*ssum)
        return [amp,phase]


#-------------------------
# elegant stuff

    def runElegant(self, start,end):

        sec = self.Facility.getSection(end)
        path = sec.mapping

        self.line = self.Facility.BeamPath(path)
        self.line.setRange(start,end)


        if 'SAT' in end:
            ang = self.Facility.getRegExpElement('S20SY02', 'MKAC', 'design_kick')
            print(ang)
            self.Facility.setRegExpElement('S20SY02', 'MKAC0.0', 'cory', ang[0])
            ang = self.Facility.getRegExpElement('S20SY02', 'MKDC', 'design_kick')
            print(ang)
            self.Facility.setRegExpElement('S20SY02', 'MKDC0.0', 'cory', ang[0])
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 2)
        else:
            self.Facility.setRegExpElement('S20SY02', 'MK.C0.0', 'cory', 0)
            self.Facility.setRegExpElement('S20SY02', 'MBND', 'angle', 0)


#        filepath='/afs/psi.ch/intranet/SF/Beamdynamics/Sven/BD_LongTrack'
        filepath='/sf/data/applications/BD-LongTrack'

        self.elegant.openLatticeStream(filepath,start)
        self.line.writeLattice(self.elegant, self.Facility)
        self.elegant.closeLatticeStream()


        f1=open(filepath+'/Lattice.ele','r')
        f2=open('%s/%s.ele' %(filepath,start),'w')
        for line in f1.readlines():
            f2.write(line.replace('REPLACE',start))
        f1.close()
        f2.close()
        
        os.system('ssh sf-lc7.psi.ch "cd %s && bash runElegant.sh %s.ele"' % (filepath,start))
        res=os.popen('ssh sf-lc7.psi.ch "cd %s && sdds2stream %s.mat -col=R56,T566,U5666"' % (filepath,start)).read()
        lines=res.splitlines()
        return lines[-1].split(' ')
