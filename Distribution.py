import os
import copy

from subprocess import run
import numpy as np
import h5py




class Distribution:
    def __init__(self):

        self.hasDistribution=False
        self.x=None
        self.y=None
        self.xslice=None
        self.yslice=None
        self.xsave=None
        self.ysave=None
        self.locsave='Start'
        self.location='Start'

    def resetDistribution(self):
        if self.hasDistribution is False:
            return
        self.x=copy.deepcopy(self.xsave)
        self.y=copy.deepcopy(self.ysave)
        self.location=copy.deepcopy(self.locsave)


    def setReference(self):
        if self.hasDistribution is False:
            return
        self.xsave=copy.deepcopy(self.x)
        self.ysave=copy.deepcopy(self.y)
        self.locsave=copy.deepcopy(self.location)


    def getCurrent(self,Q,N):
        if self.hasDistribution is False:
            return
        self.yslice,self.xslice=np.histogram(self.x,bins=N)
        ds=self.xslice[1]-self.xslice[0]
        self.xslice=self.xslice[:-1]+ds
        self.yslice=self.yslice*Q/ds*3e8/len(self.x)


    def getEnergyDistribution(self,N):
        if self.hasDistribution is False:
            return
        self.yslice,self.xslice=np.histogram(self.y,bins=N)
        ds=self.xslice[1]-self.xslice[0]
        self.xslice=self.xslice[:-1]+ds
        self.yslice=self.yslice/ds/len(self.x)


    def generateDist(self,duration,rfcurve,dist,loc,N):
        
        slen=duration*1e-12*3e8
        scl=0
        if rfcurve:
            scl=1

        if 'Flat' in dist:
            sfull=np.sqrt(12)*slen
            self.x=np.random.uniform(0,sfull,N)-0.5*sfull
        else:
            x=np.random.normal(0,slen,N)
            xheat=np.random.normal(0,10/511,N)
            idx=np.argwhere(np.abs(x)<2.5*slen)
            self.x=x[idx]
            yheat=xheat[idx]


 

        self.y=140/0.511*np.cos(scl*self.x/0.1*2*np.pi)
        self.y+=yheat

        self.hasDistribution=True
        self.location=loc
        self.setReference()

        return True

    def importMeasurement(self,image,xbb,ybb,Nsam,Thold,E0,location):

        nx=image.shape[0]
        ny=image.shape[1]
        n=nx*ny
        image=image-np.min(image)
        dist=image.reshape((n))
        tl=np.max(dist)*1e-2*Thold
        index=np.argwhere(dist<tl)
        dist[index]=1e-8
        for i in range(1,n):
            dist[i]+=dist[i-1]
        dist=dist/np.max(dist)


        ysam=np.array([i for i in range(n)])
       
        x =np.random.rand(Nsam)
        dx=np.random.rand(Nsam)
        dy=np.random.rand(Nsam)

        y=np.floor(np.interp(x,dist,ysam))
        iy =y % ny
        ix=(y-iy)/ny % nx
        
        self.x=ix+dx
        delx=(xbb[1]-xbb[0])*3e8*1e-15
        x0=(xbb[0])*3e8*1e-15
        self.x=self.x*delx/nx+x0


        self.y=iy+dy
        dely=(ybb[1]-ybb[0])*1e-2*E0
        y0=(ybb[0])*1e-2*E0+E0
        self.y=(self.y/ny*dely+y0)/0.511
        self.hasDistribution=True
        self.location=location
        self.setReference()

        return True

    def importElegant(self,filename,location,heat):

        idx=len(filename)-filename[::-1].find('/')
        file1=filename[:idx]
        file2=filename[idx:]
        res=os.popen('ssh sf-lc7.psi.ch "cd %s && sdds2stream %s -col=t,p"' % (file1,file2)).read()
        lines=res.splitlines()
        n=len(lines)
        self.x=np.ndarray((n,))
        self.y=np.ndarray((n,))
        for i in range(n):
            vals=lines[i].split(' ')
            self.x[i]=float(vals[0])*3e8
            self.y[i]=float(vals[1])
        self.location=location
        self.x-=np.mean(self.x)
        if heat >0:
            heat/=511
            self.y+=np.random.normal(0,heat,n)
            
        self.hasDistribution=True
        self.setReference()
        return True

